<?php

class Crypt {
    function __construct()
    {
        global $mysql;
        // for this example let's save our keys in a database.
        // the table where we should put our keys should have this structure [id, local_public_key, local_private_key, remote_public_key]
        $mysql = new PDO('mysql:dbname=DatabaseName;host=localhost;charset=utf8', 'DatabaseUser', 'DatabasePassword');
    }

    private function __exchangeKeys() {
        global $mysql;
        $box_kp = sodium_crypto_box_keypair();
        $sign_kp = sodium_crypto_sign_keypair();
        $box_secretKey = sodium_crypto_box_secretkey($box_kp);
        $box_publickey = sodium_crypto_box_publickey($box_kp);
        $sign_secretKey = sodium_crypto_sign_secretkey($sign_kp);
        $sign_publicKey = sodium_crypto_sign_publickey($sign_kp);
        $res = $mysql->prepare("insert into keys(local_public_key,local_private_key,remote_public_key) values (:pk,:sk,:rpk)");
        $file = file_get_contents("php://input");
        $b = json_decode($file,true);
        $res->execute(array(':pk' => base64_encode($box_publickey),':sk' => base64_encode($box_secretKey),':rpk' => $b['publickey']));
        echo json_encode(array('remote_public_key'=>base64_encode($box_publickey)));
    }

    private function __encrypt($msg,$publicKey) {
        // Assume publicKey is base64 encoded.
        return base64_encode(sodium_crypto_box_seal($msg,base64_decode($publicKey)));
    }
    
    private function __decrypt($data) {
        global $mysql;
        // Assume remote json object would be {msg : encryptedJSONMsg, publickey: remotePublicKey}
        $remotePublicKeyWithEncMsg = json_decode($data,true);
        // let's get local secret key
        $res = $mysql->prepare("select * from keys where remote_public_key=:rpk order by id desc limit 1");
        $res->execute(array(':rpk' => $remotePublicKeyWithEncMsg['publickey']));
        $row = $res->fetch(PDO::FETCH_ASSOC);
        $kp = sodium_crypto_box_keypair_from_secretkey_and_publickey(base64_decode($row['local_private_key']),base64_decode($row['local_public_key']));
        $x = json_decode(sodium_crypto_box_seal_open(base64_decode($remotePublicKeyWithEncMsg['msg']),$kp),true);
        $x['publickey'] = $row['remote_public_key'];
        return $x;
    }

    public function myCases() {
        switch ($_GET['do']) {
            case 'enctoenc':
                $de = json_decode($this->__decrypt(file_get_contents("php://input")),true);
                // Do whatever you want with $de array, then encrypt the output
                // let's say that you sent a login request to the server and $de would be => array('username' => 'xxx','password' => 'yyy')
                // Now you want to check with database and let's says it's correct and you want to return array('auth' => true) with extra params.
                echo $this->__encrypt((array('auth' => true)),$de['publickey']);
            break;
            case 'encryptmsg':
                $x = array('msg' => 'Hello, This is test msg, you should read this json from remote');
                $f = json_decode(file_get_contents("php://input"),true);
                echo $this->__encrypt(json_encode($x),$f['publickey']);
            break;
            case 'getmsg':
                $de = $this->__decrypt(file_get_contents("php://input"));
                echo json_encode($de);
            break;
            case 'exchangekeys':
                $this->__exchangeKeys();
            break;
            default:
                http_response_code(403);
            break;
        }
    }
    
}