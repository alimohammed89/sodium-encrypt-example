package com.esite_iq.cryptexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.esite_iq.cryptexample.Objects.Keys;
import com.esite_iq.cryptexample.Objects.TaskRunner;
import com.esite_iq.cryptexample.Utils.Utils;

import org.json.JSONObject;
import org.libsodium.jni.keys.KeyPair;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Keys keys;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupLayout();
        prepareLocalKeys();
    }

    private void setupLayout() {
        Button exchange = findViewById(R.id.exchange);
        Button enctoclear = findViewById(R.id.enctoclear);
        Button enctoenc = findViewById(R.id.enctoenc);
        exchange.setOnClickListener(this);
        enctoclear.setOnClickListener(this);
        enctoenc.setOnClickListener(this);
        enctoclear.setEnabled(false);
        enctoenc.setEnabled(false);
    }
    private void prepareLocalKeys() {
        KeyPair keyPair = new KeyPair();
        keys = new Keys(keyPair.getPublicKey().toBytes(),keyPair.getPrivateKey().toBytes());
    }

    @Override
    public void onClick(View view) {
        String endPoint;
        boolean clearTextRequest;
        boolean clearTextResponse;
        Map<String,Object> map = new HashMap<>();
        String mymsg="";
        switch (view.getId()) {
            case R.id.exchange:
                clearTextRequest = true;
                clearTextResponse = true;
                endPoint = "?do=exchangekeys";
                mymsg = Base64.encodeToString(keys.getLocalPublicKey(),Base64.DEFAULT);
                break;
            case R.id.enctoclear:
                clearTextRequest = false;
                clearTextResponse = true;
                endPoint = "?do=getmsg";
                map.put("msg","This is my msg");
                break;
            case R.id.enctoenc:
                clearTextRequest = false;
                clearTextResponse = false;
                endPoint = "?do=enctoenc";
                map.put("msg","This is my msg");
                break;
            default:
                return;
        }
        JSONObject o = new JSONObject(map);
        mymsg = view.getId() == R.id.exchange ? mymsg : o.toString();
        new TaskRunner().executeAsync(new Utils.httpRequest(clearTextRequest, clearTextResponse, false, endPoint, mymsg, keys), new TaskRunner.Callback<String>() {
            @Override
            public void onComplete(String result) {
                try {
                    if (view.getId() == R.id.exchange) {
                        keys.setRemotePublicKey(Base64.decode(result,Base64.DEFAULT));
                    }
                    Log.i("HttpResponse", result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail() {
                Toast.makeText(MainActivity.this,"connection issue",Toast.LENGTH_SHORT).show();
            }
        });
    }
}