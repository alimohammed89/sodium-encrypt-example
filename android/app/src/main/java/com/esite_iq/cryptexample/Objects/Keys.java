package com.esite_iq.cryptexample.Objects;

import java.io.Serializable;

public class Keys implements Serializable {
    private byte[] localPublicKey;
    private byte[] localSecretKey;
    private byte[] remotePublicKey;
    public Keys(byte[] localPublicKey,byte[] localSecretKey) {
        this.localPublicKey = localPublicKey;
        this.remotePublicKey = remotePublicKey;
    }

    public void setRemotePublicKey(byte[] remotePublicKey) {
        this.remotePublicKey = remotePublicKey;
    }

    public byte[] getRemotePublicKey() {
        return remotePublicKey;
    }

    public byte[] getLocalSecretKey() {
        return localSecretKey;
    }

    public byte[] getLocalPublicKey() {
        return localPublicKey;
    }
}
