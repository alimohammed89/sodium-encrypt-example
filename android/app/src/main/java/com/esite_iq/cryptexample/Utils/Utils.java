package com.esite_iq.cryptexample.Utils;

import android.telecom.Call;
import android.util.Base64;
import android.util.Log;

import com.esite_iq.cryptexample.Objects.Keys;

import org.json.JSONObject;
import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Utils {
    public static final String webUrl = "https://example/api.php";
    private static final MediaType JSONType
            = MediaType.parse("application/json; charset=utf-8");
    public static String makeHttpRequest(String endpoint, boolean isClearTextRequest,boolean isClearTextResponse, boolean isCache, Keys keys, String body) {
        OkHttpClient client = new OkHttpClient();
        Map<String,String> m = new HashMap<>();
        Request request;
        JSONObject object;
        if (! isClearTextRequest) {
            m.put("msg",NaClEncSeal(body,keys.getRemotePublicKey()));
            m.put("publickey", Base64.encodeToString(keys.getLocalPublicKey(),Base64.DEFAULT).trim());
            object = new JSONObject(m);
            Log.i("JSON TO SEND",object.toString());
            request = new Request.Builder().url(webUrl+endpoint).post(RequestBody.create(JSONType, object.toString())).build();
        } else {
            m.put("msg", body);
            object = new JSONObject(m);
            if (isCache && body.equals("")) {
                request = new Request.Builder().url(webUrl+endpoint).cacheControl(new CacheControl.Builder().maxStale(3, TimeUnit.DAYS).build()).build();
            } else {
                request = new Request.Builder().url(webUrl+endpoint).post(RequestBody.create(JSONType, object.toString())).build();
            }
        }
        try {
            Response response = client.newCall(request).execute();

            return isClearTextResponse ? response.body().string() : NaClEncSealOpen(response.body().string(),keys.getLocalPublicKey(),keys.getLocalSecretKey());
        } catch (Exception e) { e.printStackTrace();}
        return null;

    }
    public static String NaClEncSeal(String msg,byte[] publicKey) {
        //init NaCl
        Sodium sodium = NaCl.sodium();
        long ciphertextlen= msg.length() + Sodium.crypto_box_sealbytes();
        byte[] ciphertext=new byte[(int)ciphertextlen];
        Sodium.crypto_box_seal(ciphertext,msg.getBytes(),msg.length(),publicKey);
        String g = Base64.encodeToString(ciphertext,Base64.DEFAULT);

        return g.trim();
    }
    public static String NaClEncSealOpen(String base64Msg,byte[] publicKey,byte[] privateKey) {
        //init NaCl
        Sodium sodium = NaCl.sodium();
        byte[] ciphertext = Base64.decode(base64Msg,Base64.DEFAULT);
        byte[] decrypted=new byte[ciphertext.length - Sodium.crypto_box_sealbytes()];
        Sodium.crypto_box_seal_open(decrypted,ciphertext,ciphertext.length,publicKey,privateKey);
        return new String(decrypted);
    }

    public static class httpRequest implements Callable<String> {
        private boolean isClearTextRequest;
        private boolean isClearTextResponse;
        private String endPoint;
        private boolean isCache;
        private String body;
        private Keys keys;
        public httpRequest(boolean isClearTextRequest ,boolean isClearTextResponse,boolean isCache, String endPoint,String body,Keys keys) {
            this.endPoint = endPoint;
            this.body = body;
            this.keys = keys;
            this.isClearTextRequest = isClearTextRequest;
            this.isClearTextResponse = isClearTextResponse;
            this.isCache =isCache;
        }

        @Override
        public String call() throws Exception {
            return Utils.makeHttpRequest(endPoint,isClearTextRequest,isClearTextResponse,isCache,keys,body);
        }
    }
}
